 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/
"use strict";

let done = new Array();

function start()
{
  surveille();
}

function surveille()
{
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
    {
      let r = xhr.responseText.trim().split(",");
      let nb = done.length;
      for (let i=0; i<r.length; i++)
      {
        let v = r[i].trim();
        if (v != "" && !isNaN(parseFloat(r[i])) && isFinite(r[i]))
        {
          if (done.indexOf(v) == -1)
          {
            done.push(v);            
          }
        }
      }
      if (nb != done.length)
      {
        // on met à jour les classes pour les couleurs de fond
        for (let i=0; i<done.length; i++)
        {
          let cl = "coul0";
          if (i == done.length-1) cl = "coul2";
          else if (i == done.length-2) cl = "coul1";
          let els = document.getElementsByClassName("i"+done[i]);
          for (let j=0; j< els.length; j++)
          {
            if (j==0) console.log(els[j].className);
            if (els[j].className.indexOf(cl) == -1) els[j].className = "case i" + done[i] + " " + cl;
          }
        }
      }
      
      // on attend un peu, et on relance un test
      setTimeout(surveille, 5000);
    }
  };
  let ligne = "get";
  xhr.open("POST", "admin.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}
