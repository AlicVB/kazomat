 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/
"use strict";

let imgs = new Array();
let state = 0;
let wait = 30;
let liste = "";

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function shuffle(a)
{
   var j = 0;
   var valI = '';
   var valJ = valI;
   var l = a.length - 1;
   while(l > -1)
   {
		j = Math.floor(Math.random() * l);
		valI = a[l];
		valJ = a[j];
		a[l] = valJ;
		a[j] = valI;
		l = l - 1;
	}
	return a;
}

function update_compteur(v)
{
  console.log(v);
  if (v == "") liste = "";
  else
  {
    //on récupère le numéro du fichier image
    let vv = v.slice(7,-4);
    console.log(vv);
    if (liste == "") liste = vv;
    else liste = liste + "," + vv;
  }
  
  let xhr = new XMLHttpRequest();
  let ligne = "compteur=" + liste;
  xhr.open("POST", "kazo.php" , true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.send(ligne);
}

function start(w)
{
  wait = w*1000;
  for (let i=1; i<=20; i++)
  {
    imgs.push("images/" + i + ".svg");
  }
  
  shuffle(shuffle(imgs));
}

function controle_click(e)
{
  if (state == 0)
  {
    // premier démarrage
    e.src = "media-playback-pause.svg";
    state = 1;
    lance();
  }
  else if (state == 1)
  {
    // pause
    e.src = "media-playback-start.svg";
    state = 2;
  }
  else
  {
    // relance
    e.src = "media-playback-pause.svg";
    state = 1;
  }
}

async function lance()
{
  let e = document.getElementById("image");
  let s = document.getElementById("son");
  let sf = document.getElementById("sonfin");
  
  update_compteur(""); // remise à 0
  
  s.play();
  await sleep(2500);
  for (let i=0; i<20; i++)
  {
    update_compteur(imgs[i]);
    e.style.backgroundImage = "url(" + imgs[i] + ")";
    // on fait des pauses en plusieurs fois
    if (i<19)
    {
      await sleep(wait-2500);
      while (state != 1) await sleep(2);
      s.play();
      await sleep(2500);
    }
    else
    {
      //c'est la fin
      await sleep(wait*1.5);
      while (state != 1) await sleep(2);
      sf.play();
      e.src = "";
      update_compteur("images/0.svg");
    }
  }
}
