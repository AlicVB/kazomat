<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
    *
    * TODO
    * - vérifier présence de l'admin avant de lancer...
*/
exit; // on ne veut pas ça pour une version de production car les sessions ne sont pas gérées
if (isset($_POST['get']))
{
  if (!file_exists("compteur.txt")) echo "";
  echo file_get_contents("compteur.txt");
  exit;
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta name="mobile-web-app-capable" content="yes">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>kazomat</title>
  <link rel="stylesheet" href="admin.css">
  <script type="text/javascript" src="admin.js"></script>
</head>

<body onload="start()">
  <table class="main">
    <tr>
      <td class="bloc">
        <img src="lettres/11.svg" /><img src="lettres/1.svg" />
        <table>
          <tr><td class="case i11"></td><td class="case i18"></td><td class="case i2"></td></tr>
          <tr><td class="case i15"></td><td class="case i17"></td><td class="case i3"></td></tr>
          <tr><td class="sep" colspan="3"></td></tr>
          <tr><td class="case i20"></td><td class="case i6"></td><td class="case i4"></td></tr>
          <tr><td class="case i9"></td><td class="case i8"></td><td class="case i19"></td></tr>
        </table>
      </td>
      <td class="bloc">
        <img src="lettres/12.svg" /><img src="lettres/2.svg" />
        <table>
          <tr><td class="case i13"></td><td class="case i7"></td><td class="case i1"></td></tr>
          <tr><td class="case i19"></td><td class="case i9"></td><td class="case i8"></td></tr>
          <tr><td class="sep" colspan="3"></td></tr>
          <tr><td class="case i4"></td><td class="case i20"></td><td class="case i6"></td></tr>
          <tr><td class="case i3"></td><td class="case i16"></td><td class="case i17"></td></tr>
        </table>
      </td>
      <td class="bloc">
        <img src="lettres/13.svg" /><img src="lettres/3.svg" />
        <table>
          <tr><td class="case i7"></td><td class="case i1"></td><td class="case i13"></td></tr>
          <tr><td class="case i14"></td><td class="case i5"></td><td class="case i10"></td></tr>
          <tr><td class="sep" colspan="3"></td></tr>
          <tr><td class="case i18"></td><td class="case i2"></td><td class="case i11"></td></tr>
          <tr><td class="case i17"></td><td class="case i3"></td><td class="case i15"></td></tr>
        </table>
      </td>
    </tr>
    <tr><td class="sep_blocs"></td></tr>
    <tr>
      <td class="bloc">
        <img src="lettres/14.svg" /><img src="lettres/4.svg" />
        <table>
          <tr><td class="case i20"></td><td class="case i15"></td><td class="case i11"></td></tr>
          <tr><td class="case i5"></td><td class="case i1"></td><td class="case i8"></td></tr>
          <tr><td class="sep" colspan="3"></td></tr>
          <tr><td class="case i16"></td><td class="case i12"></td><td class="case i9"></td></tr>
          <tr><td class="case i4"></td><td class="case i3"></td><td class="case i2"></td></tr>
        </table>
      </td>
      <td class="bloc">
        <img src="lettres/15.svg" /><img src="lettres/5.svg" />
        <table>
          <tr><td class="case i12"></td><td class="case i16"></td><td class="case i6"></td></tr>
          <tr><td class="case i19"></td><td class="case i13"></td><td class="case i10"></td></tr>
          <tr><td class="sep" colspan="3"></td></tr>
          <tr><td class="case i9"></td><td class="case i7"></td><td class="case i14"></td></tr>
          <tr><td class="case i2"></td><td class="case i5"></td><td class="case i4"></td></tr>
        </table>
      </td>
      <td class="bloc">
        <img src="lettres/16.svg" /><img src="lettres/6.svg" />
        <table>
          <tr><td class="case i14"></td><td class="case i18"></td><td class="case i4"></td></tr>
          <tr><td class="case i3"></td><td class="case i7"></td><td class="case i17"></td></tr>
          <tr><td class="sep" colspan="3"></td></tr>
          <tr><td class="case i10"></td><td class="case i2"></td><td class="case i9"></td></tr>
          <tr><td class="case i13"></td><td class="case i5"></td><td class="case i20"></td></tr>
        </table>
      </td>
    </tr>
    <tr><td class="sep_blocs"></td></tr>
    <tr>
      <td class="bloc">
        <img src="lettres/17.svg" /><img src="lettres/7.svg" />
        <table>
          <tr><td class="case i11"></td><td class="case i8"></td><td class="case i15"></td></tr>
          <tr><td class="case i1"></td><td class="case i20"></td><td class="case i5"></td></tr>
          <tr><td class="sep" colspan="3"></td></tr>
          <tr><td class="case i18"></td><td class="case i12"></td><td class="case i7"></td></tr>
          <tr><td class="case i16"></td><td class="case i14"></td><td class="case i10"></td></tr>
        </table>
      </td>
      <td class="bloc">
        <img src="lettres/18.svg" /><img src="lettres/8.svg" />
        <table>
          <tr><td class="case i17"></td><td class="case i16"></td><td class="case i6"></td></tr>
          <tr><td class="case i19"></td><td class="case i18"></td><td class="case i12"></td></tr>
          <tr><td class="sep" colspan="3"></td></tr>
          <tr><td class="case i7"></td><td class="case i4"></td><td class="case i14"></td></tr>
          <tr><td class="case i1"></td><td class="case i9"></td><td class="case i3"></td></tr>
        </table>
      </td>
      <td class="bloc">
        <img src="lettres/19.svg" /><img src="lettres/9.svg" />
        <table>
          <tr><td class="case i19"></td><td class="case i8"></td><td class="case i11"></td></tr>
          <tr><td class="case i6"></td><td class="case i15"></td><td class="case i2"></td></tr>
          <tr><td class="sep" colspan="3"></td></tr>
          <tr><td class="case i20"></td><td class="case i1"></td><td class="case i10"></td></tr>
          <tr><td class="case i12"></td><td class="case i5"></td><td class="case i13"></td></tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
