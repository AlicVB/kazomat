<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
    *
    * TODO
    * - vérifier présence de l'admin avant de lancer...
*/
exit; // on ne veut pas ça pour une version de production car les sessions ne sont pas gérées
//si c'est juste pour updater le compteur
if (isset($_POST['compteur']))
{
  file_put_contents("compteur.txt", $_POST['compteur']);
  exit;
}

$wait = 30;
if (isset($_GET['wait'])) $wait = intval($_GET['wait']);

if (file_exists("compteur.txt")) unlink("compteur.txt");
?>

<!DOCTYPE html>
<html>
<head>
  <meta name="mobile-web-app-capable" content="yes">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>kazomat</title>
  <link rel="stylesheet" href="kazo.css">
  <script type="text/javascript" src="kazo.js"></script>
</head>

<body onload="start('<?php echo $wait ?>')">
  <div class="full">
    <div id="controles">
      <img src="kazo.svg" id="kazo_img" />
      <img src="media-playback-start.svg" id="controle" onclick="controle_click(this)"/>
      <audio id="son" src="son.mp3"></audio>
      <audio id="sonfin" src="son_fin.mp3"></audio>
    </div>
    <div id="image">&nbsp;</div>
    <div id="bas">
    </div>
  </div>
</body>
</html>
