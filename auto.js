 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) A.RENAUDIN  Developer

    The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
*/
"use strict";

let imgs = new Array();
let state = 0;
let liste = "";
let done = new Array();

function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  let expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let ca = document.cookie.split(';');
  for(let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function shuffle(a)
{
   var j = 0;
   var valI = '';
   var valJ = valI;
   var l = a.length - 1;
   while(l > -1)
   {
		j = Math.floor(Math.random() * l);
		valI = a[l];
		valJ = a[j];
		a[l] = valJ;
		a[j] = valI;
		l = l - 1;
	}
	return a;
}

function wait_change(e)
{
  setCookie("freq", e.value, 100);
}

function start()
{
  // initialize ferq with last used value
  let wait = getCookie("freq");
  if (wait != "") document.getElementById("wait").value = wait;

  for (let i=1; i<=20; i++)
  {
    imgs.push(i);
  }

  shuffle(shuffle(imgs));
}

function controle_click(e)
{
  if (state == 0)
  {
    // premier démarrage
    e.src = "media-playback-pause.svg";
    state = 1;
    lance();
  }
  else if (state == 1)
  {
    // pause
    e.src = "media-playback-start.svg";
    state = 2;
  }
  else
  {
    // relance
    e.src = "media-playback-pause.svg";
    state = 1;
  }
}

async function lance()
{
  let e = document.getElementById("image");
  let s = document.getElementById("son");
  let sf = document.getElementById("sonfin");

  let we = document.getElementById("wait");

  change(0);
  s.play();
  await sleep(2500);
  for (let i=0; i<20; i++)
  {
    change(i);
    // on fait des pauses en plusieurs fois
    if (i<19)
    {
      let wait = we.value * 1000;
      await sleep(wait-2500);
      while (state != 1) await sleep(2);
      s.play();
      await sleep(2500);
    }
    else
    {
      //c'est la fin
      let wait = we.value * 1000;
      await sleep(wait*1.5);
      while (state != 1) await sleep(2);
      sf.play();
      e.src = "";
    }
  }
}

function change(i)
{
  let e = document.getElementById("image");
  // on met à jour l'image principale
  e.src = "images/" + imgs[i] + ".svg";

  // on met à jour les classes pour les couleurs de fond
  if (i>=0)
  {
    let els = document.getElementsByClassName("i"+imgs[i]);
    for (let j=0; j< els.length; j++)
    {
      els[j].className = "case i" + imgs[i] + " coul2";
    }
  }
  if (i>=1)
  {
    let els = document.getElementsByClassName("i"+imgs[i-1]);
    for (let j=0; j< els.length; j++)
    {
      els[j].className = "case i" + imgs[i-1] + " coul1";
    }
  }
  if (i>=2)
  {
    let els = document.getElementsByClassName("i"+imgs[i-2]);
    for (let j=0; j< els.length; j++)
    {
      els[j].className = "case i" + imgs[i-2] + " coul0";
    }
  }
}